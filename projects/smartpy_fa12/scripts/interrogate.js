const conseiljs = require('conseiljs');
const method = "mint"

async function interrogateContract() {
const contractParameters = `parameter (or (or (or (or (or (or (or (or (or (pair %approve (pair (int %amount) (address %f)) (address %t)) (pair %burn (address %address) (int %amount))) (address %getAdministrator)) (pair %getAllowance (pair %arg (address %owner) (address %spender)) (address %target))) (pair %getBalance (address %arg) (address %target))) (address %getTotalSupply)) (pair %mint (address %address) (int %amount))) (address %setAdministrator)) (bool %setPause)) (pair %transfer (pair (int %amount) (address %f)) (address %t)));`

    const entryPoints = await conseiljs.TezosContractIntrospector.generateEntryPointsFromParams(contractParameters);
    console.log("")
    console.log("")
    entryPoints.forEach(p => {
        console.log(p.name)
        console.log(p.generateSampleInvocation())
        console.log("-  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  -  ")



    });
}

interrogateContract();