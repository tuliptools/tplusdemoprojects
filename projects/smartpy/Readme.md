# Smartpy Starter Project

Get started with SmartPy!

Read more on SmartPy here: https://smartpy.io/

## Get Started

This Project will run out of the box with a default Tplus Sandbox, as long as you have
selected a protocol other than Genesis when creating the Sandbox.

You can run this on other networks too, if you do make sure the variables currently set
point to real Addresses in your Wallet ( default settings are bootstrap1 etc )

## Using the Tplus CLI Tool

Tplus comes with a CLI tool to help you develop and test on your local machine,
to install it please see instructions @ https://tplus.dev/tplus/cli-tool

Once you have it installed you can clone this project with

```
$ tplus project init <project-id>
```

and run tasks directly from your terminal with

```
$ tplus <taskname>
```