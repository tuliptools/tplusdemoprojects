import smartpy as sp

class MyContract(sp.Contract):
    def __init__(self, myParameter1):
        self.init(myParameter1 = myParameter1)

    @sp.entry_point
    def myEntryPoint(self, params):
        self.data.myParameter1 += params