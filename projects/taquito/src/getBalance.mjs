import Taquito from '@taquito/taquito';

var Tezos = Taquito.Tezos

Tezos.setProvider({ rpc: 'http://node:8732' });
Tezos.tz
    .getBalance(process.env.TPLUS_BALANCEEXAMPLE_HASH)
    .then(balance => console.log(`${balance.toNumber() / 1000000} ꜩ`))
    .catch(error => console.log(JSON.stringify(error)));